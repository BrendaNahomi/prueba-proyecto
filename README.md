#### Librerias instaladas:
* react-router-dom version 6.2.1
* axios version 0.25.0
* tailwindcss version 3.0.22
* autoprefixer version 10.4.2
* postcss-cli version 9.1.0
* react-icons version 4.3.1
* @material-ui/core version 4.12.3
* @material-ui/icons version 4.11.2
* @material-ui/lab version 4.0.0-alpha.60
* @reduxjs/toolkit version 1.7.2
* swiper version 8.0.6
